<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/Thinkphp_admin/Public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="/Thinkphp_admin/Public/css/flat-ui.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/Thinkphp_admin/Public/js/remind.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/html5shiv.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/respond.min.js"></script>
    <![endif]-->
    <script src="/Thinkphp_admin/Public/js/vendor/jquery.min.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/video.js"></script>
    <script src="/Thinkphp_admin/Public/js/flat-ui.min.js"></script>
    
</head>
<body>



    <div style="width:90%;margin: 0 auto;">
        <h3>创建数据模型</h3>

        <form action="<?php echo U('action');?>" method="post" class="form-inline">
            <div class="form-group" style="padding-bottom: 15px;">
                <code>请选择项目的模块:</code>
                <select name="models" class="form-control select select-default">
                    <?php if(is_array($models)): foreach($models as $key=>$v): ?><option value="<?php echo ($v["name"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
                </select>
            </div>
            <div class=" pull-right">
                <a href="javascript:" class="fui-plus" id="add_field">&nbsp;添加字段</a>
            </div>
            <br/>
            <code>数据库(模块)信息:</code>

            <div class="well">
                <div class="row">
                    <div class="form-group">
                        <label for="table_name">数据表(模块英文)名称 : </label>
                        <input type="text" name="table_name" required placeholder="请输入表名称" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="table_comment">数据表(模块中文名称)注释 : </label>
                        <input type="text" name="table_comment" required placeholder="请输入表注释" class="form-control"/>
                    </div>
                    <div class="database" style="display: inline-block">
                        表引擎 :
                        <select name="engine" class="form-control select select-default">
                            <option value="INNODB">INNODB</option>
                            <option value="MYISAM">MYISAM</option>
                        </select>
                    </div>
                </div>
            </div>
            <code>创建类型:</code>

            <div class="well">
                <label>数据库:</label>
                <input type="checkbox" checked data-toggle="switch" name="databases" id="switch-database" value="1"/>
                <label>控制器:</label>
                <input type="checkbox" checked data-toggle="switch" name="controller" id="switch-controller" value="1"/>
                <label>模型:</label>
                <input type="checkbox" checked data-toggle="switch" name="model" id="switch-model" value="1"/>
                <label>视图:</label>
                <input type="checkbox" checked data-toggle="switch" name="view" id="switch-view" value="1"/>
            </div>
            <code>字段信息:</code>
            <table class="table well">
                <tr>
                    <th>字段(表单)名称</th>
                    <th class="database">字段类型</th>
                    <th class="database">长度</th>
                    <th>表单类型</th>
                    <th>默认值</th>
                    <th class="database">索引</th>
                    <th class="database">A_I</th>
                    <th>注释(表单名称)</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
 $databases_filed_type = C('DATABASES_FILED_TYPE'); $input_type = C('INPUT_TYPES'); ?>
<tr class="template">
    <td><input type="text" name="fields[]" required class="form-control" placeholder="请输入字段名称"/></td>
    <td class="database">
        <div class="form-group">
            <select name="types[]" class="form-control select select-default">
                <?php if(is_array($databases_filed_type)): foreach($databases_filed_type as $key=>$d): ?><option value="<?php echo ($d); ?>"><?php echo ($d); ?></option><?php endforeach; endif; ?>
            </select>
        </div>
    </td>
    <td class="database"><input type="text" name="lengths[]" class="form-control" placeholder="长度"/></td>
    <td>
        <select name="form[]" class="form-control select select-default">
            <?php if(is_array($input_type)): foreach($input_type as $key=>$i): ?><option value="<?php echo ($i); ?>"><?php echo ($i); ?></option><?php endforeach; endif; ?>
        </select>
    </td>
    <td><input type="text" name="defaults[]" class="form-control" placeholder="请字段类型的类型相符"/></td>
    <td class="database">
        <select name="indexs[]" class="form-control select select-default" >
            <option value="">无索引</option>
            <option value="primary key">primary</option>
            <option value="unique">unique</option>
            <option value="index">index</option>
        </select>
    </td>
    <td class="database">
        <select name="ai[]" class="form-control select select-default">
            <option value="0">无</option>
            <option value="1">自增长</option>
        </select>
    </td>
    <td><input type="text" name="comment[]" class="form-control" required placeholder="字段注释"/></td>
    <td><a href="javascript:" class="fui-cross" onclick="romove_row(this)"></a></td>
</tr>
<script type="text/javascript">
    //下拉框
    $("select").select2();
    //自动增长，自动赋值
    $('select[name="indexs[]"]').change(function(){
        if($(this).val()=='primary key'){
            $(this).parents('tr').find('select[name="ai[]"]').val(1).change();
        }else{
            $(this).parents('tr').find('select[name="ai[]"]').val(0).change();
        }
    });
</script>
            </table>
            <button class="btn btn-primary btn-wide" onclick="return beforeSubmit()">Save</button>
        </form>
    </div>



    <script type="text/javascript">
        //下拉框
        $("select").select2();
        //开关
        $('#switch-database').bootstrapSwitch().parent('div').click(function () {
            showDatabaseInfo();
        });
        $('#switch-controller').bootstrapSwitch();
        $('#switch-model').bootstrapSwitch();
        $('#switch-view').bootstrapSwitch();
        //点击添加增加一行
        $('#add_field').click(function () {
            $.ajax({
                url: "<?php echo U('addrow');?>",
                success: function (r) {
                    $('table').append(r);
                    showDatabaseInfo();
                }
            });
        });

        //判断是否创建数据库的标示,数据库字段显示隐藏处控制
        var databases = 1 ;

        //删除一行
        function romove_row(obj) {
            $(obj).parents('.template').remove();
        }
        //提交前提示信息
        function beforeSubmit() {
            //删除隐藏的字段
            $("td:hidden").remove();
            //判断是否又主键
            if(databases==1){
                var flag = false ;
                $('select[name="indexs[]"]').each(function(){
                    if($(this).val()=='primary key'){
                        flag = true ;
                    }
                });
                if(!flag){
                    alert('您没有创建主键信息【索引为：primary】');
                    return false;
                }
            }
            if (!confirm('请检查您要创建的类型，选择创建将会覆盖，默认值和字段类型必须符合。')) {
                return false;
            }
        }

        //点击数据库按钮或者加载完一行数据，判断是否显示数据库字段信息
        function showDatabaseInfo() {
            if ($('#switch-database')[0].checked) {
                databases = 1 ;
                $('.database').show();
            } else {
                databases = 0 ;
                $('.database').hide();
            }
        }
    </script>

</body>
</html>