<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        黑名单列表页
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/Thinkphp_admin/Public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="/Thinkphp_admin/Public/css/flat-ui.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/Thinkphp_admin/Public/js/remind.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/html5shiv.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/respond.min.js"></script>
    <![endif]-->
    <script src="/Thinkphp_admin/Public/js/vendor/jquery.min.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/video.js"></script>
    <script src="/Thinkphp_admin/Public/js/flat-ui.min.js"></script>
    
</head>
<body>

    <div class="clearfix well-sm">
        <ol class="breadcrumb pull-left">
          <li><a href="<?php echo U('Index/index');?>">首页</a></li>
          <li><a href="javascript:">黑名单列表</a></li>
        </ol>
        <a class='btn btn-primary btn-wide pull-right fui-plus' href="<?php echo U('edit',array('page'=>$page,'rows'=>$rows));?>" >&nbsp;添加黑名单</a>
    </div>
    
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>用户id</th>
<th>拉入黑名单原因</th>

                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <?php
 $page = I("p", false) ? I("p") : I("page"); $page = empty($page) ? 1 : $page; $rows = I("rows", 15); $table = Black_list; $data = D("$table")->page("$page,$rows")->select(); foreach($data as $k=>$v): ?><tr>
                    <td><?php echo ($v["uid"]); ?></td>
<td><?php echo ($v["content"]); ?></td>

                    <td>
                        <a class="fui-new" href="<?php echo U('edit',array('id'=>$v['id'],'page'=>$page,'rows'=>$rows));?>">&nbsp;编辑</a>
                        <a class="fui-cross" href="<?php echo U('del',array('id'=>$v['id'],'page'=>$page,'rows'=>$rows));?>">&nbsp;删除</a>
                    </td>
                </tr><?php
 if(!is_array($v)){ break ; } endforeach; $total = M("$table")->where("")->join("")->count(); $Page = new \Think\Page($total,$rows); $pageStr = $Page->show(); ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php echo ($page_str); ?>
    </div>


</body>
</html>