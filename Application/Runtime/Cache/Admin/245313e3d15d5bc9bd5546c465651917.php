<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        
 <?php
 $type = '添加'; if(isset($edit) && !empty($edit)){ $type = '修改'; } ?>
    <?php echo ($type); ?>黑名单

    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/Thinkphp_admin/Public/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="/Thinkphp_admin/Public/css/flat-ui.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/Thinkphp_admin/Public/js/remind.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/html5shiv.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/respond.min.js"></script>
    <![endif]-->
    <script src="/Thinkphp_admin/Public/js/vendor/jquery.min.js"></script>
    <script src="/Thinkphp_admin/Public/js/vendor/video.js"></script>
    <script src="/Thinkphp_admin/Public/js/flat-ui.min.js"></script>
    
</head>
<body>

     <div class="clearfix well-sm">
        <ol class="breadcrumb pull-left">
          <li><a href="<?php echo U('Index/index');?>">首页</a></li>
          <li><a href="<?php echo U('index',array('p'=>$page,'rows'=>$rows));?>">黑名单列表</a></li>
          <li><a href="javascript:"><?php echo ($type); ?>黑名单</a></li>
        </ol>
        <a class='btn btn-primary btn-wide pull-right' href="<?php echo U('index',array('p'=>$page,'rows'=>$rows));?>" >返回黑名单列表</a>
    </div>
    <form action="<?php echo U('action');?>"  method="post" class="form-horizontal" >
        <div class='form-group'>
    <label for='uid_input' class='col-lg-2 control-label'>用户id</label>
    <div class='col-lg-9'>        <input class='form-control' type='text' name='uid' id='uid'_input value='<?php echo ($BlackListData["uid"]); ?>' />
    </div></div>
<div class='form-group'>
    <label for='content_input' class='col-lg-2 control-label'>拉入黑名单原因</label>
    <div class='col-lg-9'>        <input class='form-control' type='text' name='content' id='content'_input value='<?php echo ($BlackListData["content"]); ?>' />
    </div></div>
<div class='form-group text-center'>
    <input class='btn btn-primary btn-wide' type='submit' value='&nbsp;保存<?php echo ($type); ?>' />
</div>

        <?php if($edit): ?><input type="hidden" name="id" value="<?php echo ($BlackListData["id"]); ?>" /><?php endif; ?>
    </form>


</body>
</html>