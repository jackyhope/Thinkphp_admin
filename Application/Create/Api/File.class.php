<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2014/12/29
 * Time: 21:52
 */

namespace Create\Api;


class File
{
    /**
     * 创建文件并写入字符串
     * @param $path  文件路劲
     * @param $str 写入字符串
     * @return bool
     */
    public static function create($path, $str)
    {
        $dir = dirname($path);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if (file_exists($path)) {
            unlink($path);
        }
        $fh = fopen($path, 'a');
        if (!fwrite($fh, $str)) {
            exit("写入{$path}失败");
        }
        fclose($fh);
        return true;
    }


    /**
     * 获取到当前项目的模块名称
     * @return boolean|array
     */
    public function get_dir()
    {
        $data = array();
        if (!$dh = opendir(APP_PATH)) {
            return false;
        }
        while (($file = readdir($dh)) !== false) {
            //排除当前目录，上级目录，运行文件Runtime目录，和去掉其他文件
            if ($file === '.' || $file === '..' || $file === 'Runtime' || $file === MODULE_NAME || $file === "Common" || !is_dir(APP_PATH . $file)) {
                continue;
            }
            $data[]['name'] = $file;
        }
        closedir($dh);
        return $data;
    }
}