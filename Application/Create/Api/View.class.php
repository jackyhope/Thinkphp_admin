<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2014/12/29
 * Time: 21:45
 */

namespace Create\Api;

/**
 * 视图文件
 * Class Model
 * @package Create\Api
 */
class View
{
    private $viewName = ''; //视图名称
    private $tableName = '';
    private $comment = ''; //视图中文注释
    private $model = ''; //模块
    private $data = array();//字段信息数据
    public function __construct($name, $comment, $model,$data)
    {
        if (empty($name) || empty($model)) {
            exit('请传入模型、模块名称');
        }
        $num = strpos($name,'_');
        $tmp = substr($name,$num+1,1);
        $this->viewName = ucfirst(str_replace('_' . $tmp,strtoupper($tmp),$name));

        $this->tableName = ucfirst($name);
        $this->comment = $comment;
        $this->model = ucfirst($model);
        $this->data = $data;
    }

    /**
     * 创建文件
     * @param $arr 数据表的所有字段信息
     * @return bool
     */
    public function create()
    {
        $list_str = $this->create_index_str();//产生首页字符串
        $add_str = $this->create_add_str();//产生首页字符串
        $list_path = APP_PATH . $this->model . '/View/' . $this->viewName . '/index.html';
        $add_path = APP_PATH . $this->model . '/View/' . $this->viewName . '/edit.html';
        if (File::create($list_path, $list_str)) {
            if (!File::create($add_path, $add_str)) {
                exit('创建' . $add_path . '失败');
            }
            return true;
        } else {
            exit('创建' . $list_path . '失败');
        }

    }

    /**
     * 创建首页字符串
     * @param $arr 数据表的所有字段信息
     * @return mixed
     */
    public function create_index_str()
    {
        $arr = $this->data ;
        $pk = M($this->name)->getPk();
        $thead_str = '';
        $tbody_str = '';
        foreach ($arr as $v) {
            if ($v['fields'] === $pk) {
                continue;
            }
            $thead_str .= "<th>{$v['comment']}</th>\r\n";
            $tbody_str .= "<td>{??v.{$v['fields']}}</td>\r\n";
        }

        $str = <<<PHP
<extend name="./Public/base.html" />
<block name="title">{$this->comment}列表页</block>
<block name="body">
    <div class="clearfix well-sm">
        <ol class="breadcrumb pull-left">
          <li><a href="{:U('Index/index')}">首页</a></li>
          <li><a href="javascript:">{$this->comment}列表</a></li>
        </ol>
        <a class='btn btn-primary btn-wide pull-right fui-plus' href="{:U('edit',array('page'=>??page,'rows'=>??rows))}" >&nbsp;添加{$this->comment}</a>
    </div>
    <taglib name="lx" />
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                {$thead_str}
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <lx:list table="{$this->tableName}"  page="??page,??rows" >
                <tr>
                    {$tbody_str}
                    <td>
                        <a class="btn btn-link btn-sm" href="{:U('edit',array('id'=>??v['id'],'page'=>??page,'rows'=>??rows))}"><span class="fui-new">&nbsp;</span>编辑</a>
                        <a class="btn btn-link btn-sm" href="{:U('del',array('id'=>??v['id'],'page'=>??page,'rows'=>??rows))}"><span class="fui-cross">&nbsp;</span>删除</a>
                    </td>
                </tr>
            </lx:list>
        </tbody>
    </table>
    <div class="text-center">
        {??pageStr}
    </div>
</block>
PHP;

        return str_replace('??', '$', $str);
    }

    /**
     * 创建添加页面字符串
     * @param $arr 数据表的所有字段信息
     * @return mixed
     */
    public function create_add_str()
    {
        $arr = $this->data;
        check_table_exist($this->tableName);
        $pk = M($this->tableName)->getPk();
        //循环字段信息，创建表单
        $form_str = '';
        $enctype = '';
        foreach ($arr as $k => $v) {
            if ($v['fields'] === $pk) {
                continue;
            }
            $form_str .= "<div class='form-group'>\r\n";
            $form_str .= "    <label for='{$v['fields']}_input' class='col-lg-2 control-label'>{$v['comment']}</label>\r\n";
            $form_str .= "    <div class='col-lg-9'>";
            $form_str .= "        <input class='form-control' type='{$v['form']}' name='{$v['fields']}' id='{$v['fields']}_input' value='{??{$this->viewName}Data.{$v['fields']}}' />\r\n";
            $form_str .= "    </div>";
            $form_str .= "</div>\r\n";
            if($v['form']=='file'){
                $enctype = 'enctype="multipart/form-data"';
            }
        }
        $form_str .= "<div class='form-group text-center'>\r\n";
        $form_str .= "    <input class='btn btn-primary btn-wide' type='submit' value='&nbsp;保存{??type}' />\r\n";
        $form_str .= "</div>\r\n";

        $str = <<<PHP
<extend name="./Public/base.html" />
<block name="title">
 <?php
    ??type = '添加';
    if(isset(??edit) && !empty(??edit)){
        ??type = '修改';
    }
 ?>
    {??type}{$this->comment}
</block>
<block name="body">
     <div class="clearfix well-sm">
        <ol class="breadcrumb pull-left">
          <li><a href="{:U('Index/index')}">首页</a></li>
          <li><a href="{:U('index',array('p'=>??page,'rows'=>??rows))}">{$this->comment}列表</a></li>
          <li><a href="javascript:">{??type}{$this->comment}</a></li>
        </ol>
        <a class='btn btn-primary btn-wide pull-right' href="{:U('index',array('p'=>??page,'rows'=>??rows))}" >返回{$this->comment}列表</a>
    </div>
    <form action="{:U('action')}"  method="post" class="form-horizontal" {$enctype}>
        {$form_str}
        <if condition="??edit">
            <input type="hidden" name="{$pk}" value="{??{$this->viewName}Data.id}" />
        </if>
    </form>
</block>
PHP;

        return str_replace('??', '$', $str);
    }
}