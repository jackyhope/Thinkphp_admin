<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/1/1
 * Time: 14:54
 */

namespace Create\Api;


class Exec
{

    public $table_name = '';//表名称
    public $table_comment = '';//表注释
    public $models = '';//模块名称
    public $data = array();//字段数据
    public $engine = 'INNODB';//数据库引擎
    private $classArr = array();//存放要执行的类

    public function __construct($arr)
    {
        $this->classArr = $arr;
    }

    public function create()
    {

        foreach ($this->classArr as $v) {
            $className = 'Create\Api\\' . ucfirst($v);
            switch ($className) {
                //只有创建数据库传的参数不同，其他都一样
                case 'Create\Api\Databases':
                    $model = new $className($this->table_name, $this->table_comment, $this->engine, $this->data);
                    break;
                default:
                    $model = new $className($this->table_name, $this->table_comment, $this->models, $this->data);
                    break;
            }

            $status = $model->create();
            if (!$status) {
                exit("执行{$v}时发生错误");
            }
        }
    }


}