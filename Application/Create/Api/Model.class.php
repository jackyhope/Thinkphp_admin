<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2014/12/29
 * Time: 21:45
 */

namespace Create\Api;

/**
 * 创建模型
 * Class Model
 * @package Create\Api
 */
class Model
{
    private $tablename = ''; //数据表名称
    private $className = '' ;//模型类名
    private $comment = ''; //模型中文注释
    private $model = ''; //模块
    private $data = array();//字段信息数据
    public function __construct($name, $comment, $model,$data)
    {
        if (empty($name) || empty($model)) {
            exit('请传入模型、模块名称');
        }

        $num = strpos($name,'_');
        $tmp = substr($name,$num+1,1);

        $this->className = ucfirst(str_replace('_' . $tmp,strtoupper($tmp),$name));
        $this->tablename = ucfirst($name);
        $this->comment = $comment;
        $this->model = ucfirst($model);
        $this->data = $data;
    }

    public function create() {
        $str = $this->create_file_str();//产生字符串
        $path = APP_PATH . $this->model . '/Model/' . $this->className.'Model.class.php';
        return File::create($path,$str);

    }

    public function create_file_str()
    {
        $time = date('Y-m-d H:i:s');
        $table_name = strtolower($this->tablename) ;
        $str = <<<PHP
<?php

/**
 * {$this->comment}模型
 * TIME : {$time}
 */

namespace {$this->model}\Model;
use Think\Model;

class {$this->className}Model extends Model{

    protected ??tableName = '{$table_name}';


}
PHP;

        return str_replace('??', '$', $str);
    }
}