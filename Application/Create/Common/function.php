<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/1/27
 * Time: 13:37
 */

function check_table_exist($tableName){
    $tableName = C('DB_PREFIX') . strtolower($tableName);
    $tables = M()->query('show tables');
    if(empty($tables)){
        exit('数据库中没有表');
    }
    foreach($tables as $k=>$v){
        $index = 'tables_in_' . C('DB_NAME') ;
        if($v[$index]==$tableName){
            return true ;
        }
    }
    exit('数据库中没有 '.$tableName.' 表，请创建');
}