<?php

return array(

    //表单type属性
    'INPUT_TYPES' => array(
        'text','password','file','select','checkbox','radio','email','url','image','hidden','button'
    ),
    //数据库字段类型
    'DATABASES_FILED_TYPE'=>array(
        'int','tinyint','char','varchar','text','date','decimal','float','smallint'
    )
);
