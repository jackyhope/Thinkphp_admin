<?php

namespace Create\Controller;

use Create\Api\Exec;
use Create\Api\File;
use Think\Controller;

class IndexController extends Controller
{

    private $post = array(); //接受到的POST数据

    public function start()
    {
        //查询出当前项目的模块
        $file = new File();
        $models = $file->get_dir();
        if (empty($models)) {
            exit('请先创建好一个空模块');
        }
        $this->assign('models', $models);
        $this->display('index');
    }

    public function index(){
        header("Content-type:text/html;charset=utf-8");
        $url = U('start') ;
        $str = <<<STR
<div style="margin-left:30%;">
<pre>

                            _ooOoo_
                           o8888888o
                           88" . "88
                           (| -_- |)
                            O\ = /O
                        ____/`---'\____
                      .   ' \\| |// `.
                       / \\||| : |||// \
                     / _||||| -:- |||||- \
                       | | \\\ - /// | |
                     | \_| ''\---/'' | |
                      \ .-\__ `-` ___/-. /
                   ___`. .' /--.--\ `. . __
                ."" '< `.___\_<|>_/___.' >'"".
               | | : `- \`.;`\ _ /`;.`/ - ` : | |
                 \ \ `-. \_ __\ /__ _/ .-` / /
         ======`-.____`-.___\_____/___.-`____.-'======
                            `=---='

         .............................................
                  佛祖保佑             永无BUG
          佛曰:
                  写字楼里写字间，写字间里程序员；
                  程序人员写程序，又拿程序换酒钱。
                  酒醒只在网上坐，酒醉还来网下眠；
                  酒醉酒醒日复日，网上网下年复年。
                  但愿老死电脑间，不愿鞠躬老板前；
                  奔驰宝马贵者趣，公交自行程序员。
                  别人笑我忒疯癫，我笑自己命太贱；
                  不见满街漂亮妹，哪个归得程序员？
 -----------------------------------------------------------------

 </pre>
<h1 style="margin-left:15%;"><a href="{$url}">点击进入</a></h1>
 </div>

STR;
    echo $str ;
    }

    public function action()
    {
        $this->getPost();//接受post数据

        //需要执行的类名
        $data = $this->post;
        $execArr = array();
        $array = array('databases', 'controller', 'view', 'model');
        foreach ($data as $k => $v) {
            if (in_array($k, $array)) {
                //保证创建顺序
                switch ($k) {
                    case 'databases':
                        $execArr[0] = 'databases';
                        break;
                    case 'controller':
                        $execArr[2] = 'controller';
                        break;
                    case 'model':
                        $execArr[4] = 'model';
                        break;
                    case 'view':
                        $execArr[3] = 'view';
                        break;
                }
            }
        }
        //反转数组，数据库必须要首先创建
        ksort($execArr);
        $create = new Exec($execArr);
        $create->table_name = $this->post['table_name'];
        $create->table_comment = $this->post['table_comment'];
        $create->models = $this->post['models'];
        $create->data = $this->post['rows'];
        $create->engine = $this->post['engine'];
        $create->create();
        //下滑线转大写正确显示
        $num = strpos($data['table_name'], '_');
        $tmp = substr($data['table_name'], $num + 1, 1);
        $name = ucfirst(str_replace('_' . $tmp, strtoupper($tmp), $data['table_name']));
        echo '请访问 :  ' . $_SERVER['HTTP_HOST'] . U($data['models'] . '/' . $name . '/index');
    }

    /**
     * 在页面上点击添加一个字段的时候异步返还一个字段填写表单
     */
    public function addrow()
    {
        $this->display('rows');
    }


    /**
     * 接受POST数据
     * @return array
     */
    private function getPost()
    {
        $post = I('post.');
        $data = array('rows' => array());
        foreach ($post as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $key => $c) {
                    $data['rows'][$key][$k] = $c;
                }
            } else {
                $data[$k] = $v;
            }
        }
        $data = array_reverse($data);

        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $key => $value) {
                    $data[$k][$key] = array_map('strtolower', $value);
                }
            } else {
                $data[$k] = strtolower($v);
            }
        }
        $this->post = $data;
        return $this->post;
    }


}
